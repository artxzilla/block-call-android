package th.co.artxzilla.blockcall.presentation

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.artxzilla.blockcall.R
import th.co.artxzilla.blockcall.data.BlockCall
import th.co.artxzilla.blockcall.databinding.ItemBlockCallBinding
import th.co.artxzilla.blockcall.utils.Constants.Companion.VIEWTYPE_EMPTY
import th.co.artxzilla.blockcall.utils.Constants.Companion.VIEWTYPE_NORMAL
import kotlin.properties.Delegates

class BlockCallListAdapter : RecyclerView.Adapter<BlockCallListAdapter.ViewHolder>() {
    var dataList by Delegates.observable(listOf<BlockCall>()) { _, _, _ -> notifyDataSetChanged() }
    var onRemoveItemClickListener: ((blockCall: BlockCall) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = when (viewType) {
            VIEWTYPE_EMPTY -> LayoutInflater.from(parent.context).inflate(R.layout.no_data_item, parent, false)
            VIEWTYPE_NORMAL -> LayoutInflater.from(parent.context).inflate(R.layout.item_block_call, parent, false)
            else -> LayoutInflater.from(parent.context).inflate(R.layout.item_block_call, parent, false)
        }
        return ViewHolder(view, viewType, onRemoveItemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder.itemViewType == VIEWTYPE_NORMAL) {
            holder.binding!!.data = dataList[position]
        }
    }

    override fun getItemCount(): Int {
        return if (dataList.isEmpty()) {
            1
        } else {
            dataList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList.isEmpty()) {
            VIEWTYPE_EMPTY
        } else {
            VIEWTYPE_NORMAL
        }
    }

    class ViewHolder(view: View, viewType: Int, onRemoveItemClickListener: ((blockCall: BlockCall) -> Unit)?) : RecyclerView.ViewHolder(view) {
        var binding: ItemBlockCallBinding? = null

        init {
            if (viewType == VIEWTYPE_NORMAL) {
                binding = DataBindingUtil.bind(view)
                binding?.btnItemblockcallRemove?.setOnClickListener { onRemoveItemClickListener?.invoke(binding!!.data!!) }
            }
        }
    }
}