package th.co.artxzilla.blockcall.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import th.co.artxzilla.blockcall.R

object Utils {
    fun messageDialogUtil(context: Context, title: String?, msg: String?, callback: (() -> Unit)?) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(msg)
        builder.setPositiveButton(R.string.remove) { dialog, _ ->
            callback?.invoke()
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.cancel) { dialog, _ ->
            dialog.dismiss()
        }
        builder.setCancelable(false)
        builder.create()
        builder.show()
    }

    fun requestPermissions(activity: Activity?) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity != null) {
            Constants.PERMISSIONS
                    .filter { ActivityCompat.checkSelfPermission(activity, it) != PackageManager.PERMISSION_GRANTED }
                    .forEach { ActivityCompat.requestPermissions(activity, Constants.PERMISSIONS, 1) }
        }
    }
}
