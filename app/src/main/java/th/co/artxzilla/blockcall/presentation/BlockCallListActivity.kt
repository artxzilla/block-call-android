package th.co.artxzilla.blockcall.presentation

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import th.co.artxzilla.blockcall.Application.Companion.blockCallPreference
import th.co.artxzilla.blockcall.R
import th.co.artxzilla.blockcall.data.BlockCall
import th.co.artxzilla.blockcall.databinding.ActivityBlockCallListBinding
import th.co.artxzilla.blockcall.utils.Utils

class BlockCallListActivity : AppCompatActivity() {

    internal val binding by lazy {
        DataBindingUtil.setContentView<ActivityBlockCallListBinding>(this, R.layout.activity_block_call_list)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(BlockCallListViewModel::class.java).also {
            BlockCallListViewModel()
        }
    }

    internal val blockCallListAdapter = BlockCallListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding?.apply {
            data = BlockCall()
            activity = this@BlockCallListActivity
            viewmodel = viewModel
        }

        Utils.requestPermissions(this@BlockCallListActivity)

        initInstance()
        getBlockCallList()
    }

    private fun initInstance() {
        binding.rvBlockcalllistList.apply {
            setHasFixedSize(true)

            val linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            layoutManager = linearLayoutManager

            blockCallListAdapter.onRemoveItemClickListener = {
                viewModel.onRemoveItemClickListener(this@BlockCallListActivity, it)
            }

            adapter = blockCallListAdapter
        }

        binding.edtBlockcalllistNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding.lbBlockcalllistNumber.error = null
                binding.lbBlockcalllistNumber.isErrorEnabled = false
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
    }

    private fun getBlockCallList() {
        blockCallListAdapter.dataList = blockCallPreference.getBlockCalls()
    }
}
