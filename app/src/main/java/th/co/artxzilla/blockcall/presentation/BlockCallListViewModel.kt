package th.co.artxzilla.blockcall.presentation

import android.arch.lifecycle.ViewModel
import th.co.artxzilla.blockcall.Application.Companion.blockCallPreference
import th.co.artxzilla.blockcall.R
import th.co.artxzilla.blockcall.data.BlockCall
import th.co.artxzilla.blockcall.utils.Utils

class BlockCallListViewModel : ViewModel() {

    private val TAG = "BlockCallListViewModel"

    fun onClickBlockCallListAdd(blockCall: BlockCall, activity: BlockCallListActivity) {
        if (validateNumber(blockCall.callNumber)) {
            val mBlockCall = BlockCall(blockCallPreference.getBlockCalls().size + 1, blockCall.callNumber)
            blockCallPreference.addBlockCall(mBlockCall)
            activity.blockCallListAdapter.dataList = blockCallPreference.getBlockCalls()

            // clear input
            blockCall.callNumber = null
        } else {
            activity.binding.lbBlockcalllistNumber.error = activity.getString(R.string.validate_number)
        }
    }

    private fun validateNumber(number: String?): Boolean {
        return number != null && number.length == 10 && number[0].toString() == "0"
    }

    fun onRemoveItemClickListener(activity: BlockCallListActivity, blockCall: BlockCall) {
        Utils.messageDialogUtil(activity, null, activity.getString(R.string.confirm_remove), {
            blockCallPreference.removeBlockCall(blockCall)
            activity.blockCallListAdapter.dataList = blockCallPreference.getBlockCalls()
        })
    }
}