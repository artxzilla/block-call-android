package th.co.artxzilla.blockcall

import android.app.Application
import th.co.artxzilla.blockcall.data.BlockCallPreference

class Application : Application() {

    companion object {
        lateinit var blockCallPreference: BlockCallPreference
    }

    override fun onCreate() {
        super.onCreate()

        blockCallPreference = BlockCallPreference(applicationContext)
    }
}

