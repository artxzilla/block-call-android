package th.co.artxzilla.blockcall.data

import android.databinding.Bindable
import android.databinding.Observable
import android.databinding.PropertyChangeRegistry
import th.co.artxzilla.blockcall.BR

open class BlockCall : Observable {

    var uuid: Int = 0
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.uuid)
            }
        }

    var callNumber: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.callNumber)
            }
        }

    /* constructor */
    constructor()

    constructor(uuid: Int, callNumber: String?) {
        this.uuid = uuid
        this.callNumber = callNumber
    }

    /* from base observable */
    @Transient
    private var mCallbacks: PropertyChangeRegistry? = null

    /* from observable interface */
    @Synchronized
    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        if (mCallbacks == null) {
            mCallbacks = PropertyChangeRegistry()
        }
        mCallbacks!!.add(callback)
    }

    /* from observable interface */
    @Synchronized
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        if (mCallbacks != null) {
            mCallbacks!!.remove(callback)
        }
    }

    private fun notifyPropertyChanged(fieldId: Int) {
        if (mCallbacks != null) {
            mCallbacks!!.notifyCallbacks(this, fieldId, null)
        }
    }
}