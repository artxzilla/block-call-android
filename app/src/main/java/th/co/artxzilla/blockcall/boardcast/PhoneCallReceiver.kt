package th.co.artxzilla.blockcall.boardcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager

import com.android.internal.telephony.ITelephony
import th.co.artxzilla.blockcall.Application

class PhoneCallReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.PHONE_STATE") {
            val number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)
            if (Application.blockCallPreference.getBlockCalls().any { number == it.callNumber }) {
                disconnectPhone(context)
            }
        }
    }

    private fun disconnectPhone(context: Context) {
        val telephonyService: ITelephony
        val telephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        try {
            val method = Class.forName(telephony.javaClass.name).getDeclaredMethod("getITelephony")
            method.isAccessible = true
            telephonyService = method.invoke(telephony) as ITelephony
            telephonyService.endCall()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
