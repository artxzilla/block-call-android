package th.co.artxzilla.blockcall.utils

import android.Manifest

class Constants {
    companion object {
        val PERMISSIONS = arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE)

        const val VIEWTYPE_NORMAL = 0x0011
        const val VIEWTYPE_EMPTY = 0x0012
    }
}