package th.co.artxzilla.blockcall.data

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import com.google.gson.Gson
import java.util.*

class BlockCallPreference(private val context: Context) {

    // This four methods are used for maintaining blockCalls.
    private fun saveBlockCalls(blockCalls: List<BlockCall>) {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: Editor = settings.edit()
        val jsonBlockCalls = Gson().toJson(blockCalls)
        editor.putString(FAVORITES, jsonBlockCalls)
        editor.apply()
    }

    fun addBlockCall(blockCall: BlockCall) {
        var blockCalls: MutableList<BlockCall> = getBlockCalls()
        if (blockCalls.isEmpty())
            blockCalls = ArrayList()
        blockCalls.add(blockCall)
        saveBlockCalls(blockCalls)
    }

    fun removeBlockCall(blockCall: BlockCall) {
        val blockCalls = getBlockCalls()
        for (item in blockCalls) {
            if (item.uuid == blockCall.uuid) {
                blockCalls.remove(item)
                saveBlockCalls(blockCalls)
                break
            }
        }
    }

    fun getBlockCalls(): ArrayList<BlockCall> {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        var blockCalls: List<BlockCall>

        if (settings.contains(FAVORITES)) {
            val jsonBlockCalls = settings.getString(FAVORITES, null)
            val gson = Gson()
            val favoriteItems = gson.fromJson(jsonBlockCalls,
                    Array<BlockCall>::class.java)

            blockCalls = Arrays.asList(*favoriteItems)
            blockCalls = ArrayList(blockCalls)
        } else
            return arrayListOf()

        return blockCalls
    }

    companion object {
        val PREFS_NAME = "PRODUCT_APP"
        val FAVORITES = "BlockCall_Favorite"
    }
}
