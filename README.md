## [Android App] Block Call

- Cancels the call if any numbers on the blocked list call the phone
- Add & Remove blocked list call number
- Saves the list in Android preferences
- App should be running in the background

### License

Panupong Sathapornnuwong [panupong.satha@gmail.com], GitLab, Mar 2018